# dokuwiki

docker-compose

## deploy

```
$ git clone git@bitbucket.org:rupreht/docker-rupreht-dokuwiki.git dokuwiki
```
Create file .env

```
# Info - https://github.com/jwilder/nginx-proxy#how-ssl-support-works
SSL_POLICY=Mozilla-Old
VIRTUAL_HOST=
LETSENCRYPT_HOST=
LETSENCRYPT_EMAIL=
TZ=Asia/Yekaterinburg
MAIL_DOMAIN=
MAIL_FROM_ADDRESS=
SMTP_NAME=
SMTP_PASSWORD=
SMTP_PORT=
SMTP_HOST=
SMTP_AUTHTYPE=
SMTP_SECURE=
```
Run
```
docker-compose up -d
```